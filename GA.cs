﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace GeneticAlgorithms
{
    public class GA
    {
        private Fitness fitness;    // A function that assigns an evaluation score, given a hypothesis.
        private int fitness_threshold;  // A threshold specifying the termination criterion.
        private int p;  // The number of hypotheses to be included in the population.
        private double r;   // The fraction of the population to be replaced by Crossover at each step.
        private double m;   // The mutation rate.

        private int strLen;

        private HashSet<string> P = new HashSet<string>();

        private List<char> chars = new List<char>() {'a','b','c','d','e','f','g','h','i','j','k',
        'l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};

        private Dictionary<string, int> compute = new Dictionary<string, int>();
        private int currentMaxFit;

        private Dictionary<string, double> probabilityMap = new Dictionary<string, double>();

        public GA(Fitness fitness, int fitness_threshold, int p, double r, double m) 
        {
            this.fitness = fitness;
            this.fitness_threshold = fitness_threshold;
            this.p = p;
            this.r = r;
            this.m = m;
            setStrLen();
        }

        private void setStrLen()
        {
            this.strLen = fitness.maxFit();
        }

        public void init_P()
        {
            Random rand = new Random();
            for (int i = 0; i < p; i++)
            {
                StringBuilder myStr = new StringBuilder();
                int j = 0;
                while (j < strLen)
                {
                    int k = rand.Next(0, 26);
                    char c = chars[k];
                    myStr.Append(c);
                    j++;
                }
                P.Add(myStr.ToString());
                myStr = new StringBuilder();
            }
        }

        public void evaluate()
        {
            foreach (string s in P)
            {
                compute[s] = fitness.fit_score(s);
            }

            foreach (var it in compute)
            {
                if (it.Value >= currentMaxFit)
                    currentMaxFit = it.Value;
            }
        }

        public void runGA()
        {
            int iterations = 0;
            printMenu();
            string choice = Console.ReadLine();

            Stopwatch stopwatch = new Stopwatch();

            while (currentMaxFit < fitness_threshold)
            {
                stopwatch.Start();
                HashSet<string> Ps = new HashSet<string>();

                //--------------------- SELECT ---------------------------
                select();
                //--------------------- END SELECT -----------------------

                //--------------------- CROSSOVER TYPE -------------------
                if (choice == "1")
                    singleCrossOver(Ps);
                else if (choice == "2")
                    twoPointCross(Ps);
                else if (choice == "3")
                    uniformCross(Ps);
                //--------------------- END CROSSOVER --------------------

                int members = (int)(m * Ps.Count);      // this is the mutation rate factor
                List<string> temp = Ps.ToList();
                Random rand = new Random();

                //--------------------- MUTATION -------------------------
                for (int i = 0; i < members; i++)
                {
                    StringBuilder myStr = new StringBuilder();
                    myStr.Append(temp[i]);
                    int k = rand.Next(0, temp[i].Length);
                    int h = rand.Next(0, 26);
                    myStr[k] = chars[h];
                    temp[i] = myStr.ToString();
                }
                //-------------------- END MUTATION ----------------------

                //-------------------- UPDATE POPULATION -----------------
                P.Clear();
                foreach (string s in temp)
                    P.Add(s);
                //------------------- END UPDATE POPULATION --------------

                //------------------- EVALUATE ---------------------------
                evaluate();
                //------------------- END EVALUATE -----------------------
                iterations++;
            }
            stopwatch.Stop();
            Console.WriteLine("\nFitness_threshold reached.");
            Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);
            if (P.Contains(fitness.goalCheck()))
                Console.WriteLine("Correct guess found: {0}", fitness.goalCheck());
            Console.WriteLine("Iterations = {0}", iterations);
        }

        private void select()
        {
            int sumFit = 0;
            foreach (string s in P)
            {
                sumFit += fitness.fit_score(s);
            }

            foreach (string s in P)
            {
                probabilityMap[s] = (double)fitness.fit_score(s) / sumFit;
            }
        }

        private void singleCrossOver(HashSet<string> Ps)
        {
            int numberPairs = 0;
            numberPairs = (int)(r * p) / 2;

            List<KeyValuePair<string, double>> list = probabilityMap.ToList();
            list.Sort(compare);
            list.Reverse();

            int crossMask = list[0].Key.Length / 2;

            for (int i = 0, j = 0; i < numberPairs; i++, j+=2)
            {
                string parentOne = list[j].Key;
                string parentTwo = list[j + 1].Key;

                string offspringOne = parentOne.Substring(0, crossMask);
                offspringOne += parentTwo.Substring(crossMask);

                string offspringTwo = parentTwo.Substring(0, crossMask);
                offspringTwo += parentOne.Substring(crossMask);

                Ps.Add(offspringOne);
                Ps.Add(offspringTwo);
            }

            //foreach (string s in Ps)
                //Console.WriteLine(s);
        }

        private void twoPointCross(HashSet<string> Ps)
        {
            int pairs = (int)(r * p) / 2;

            List<KeyValuePair<string, double>> list = probabilityMap.ToList();
            list.Sort(compare);
            list.Reverse();

            int mask1 = 1;
            int mask2 = list[0].Key.Length;
            int mask3 = mask2 - mask1;

            for (int i = 0, j = 0; i < pairs; i++, j += 2)
            {
                string parentOne = list[j].Key;
                string parentTwo = list[j + 1].Key;

                string offspringOne = parentOne.Substring(0, mask1);
                offspringOne += parentTwo.Substring(mask1, mask3);
                offspringOne += parentOne.Substring(mask2);

                string offspringTwo = parentTwo.Substring(0, mask1);
                offspringTwo += parentOne.Substring(mask1, mask3);
                offspringTwo += parentTwo.Substring(mask2);

                Ps.Add(offspringOne);
                Ps.Add(offspringTwo);
            }

            //foreach (string s in Ps)
                //Console.WriteLine(s);
        }

        private void uniformCross(HashSet<string> Ps)
        {
            int pairs = (int)(r * p) / 2;

            List<KeyValuePair<string, double>> list = probabilityMap.ToList();
            list.Sort(compare);
            list.Reverse();

            for (int i = 0, j = 0; i < pairs; i++, j += 2)
            {
                string p1 = list[j].Key;
                string p2 = list[j + 1].Key;

                string o1 = "";
                string o2 = "";

                for (int k = 0, h = 1; k < list[j].Key.Length; h+=2, k+=2)
                {
                    o1 += p1[k];
                    o1 += p2[h];

                    o2 += p2[k];
                    o2 += p1[h];
                }

                Ps.Add(o1);
                Ps.Add(o2);
            }

            //foreach (string s in Ps)
                //Console.WriteLine(s);
        }

        private int compare(KeyValuePair<string, double> a, KeyValuePair<string, double> b)
        {
            return a.Value.CompareTo(b.Value);
        }

        private void printMenu()
        {
            Console.WriteLine("\nThese are the available crossover types.");
            Console.WriteLine(" 1 - Single Point");
            Console.WriteLine(" 2 - Two Point");
            Console.WriteLine(" 3 - Uniform Point");
            Console.Write("Enter choice: ");
        }
    }
}
