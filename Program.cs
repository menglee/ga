﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace GeneticAlgorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            string choice = "";
            do
            {
                Console.Write("Enter string (all lowercase a-z) for me to guess: ");
                string userInput = Console.ReadLine();
                Fitness fit = new Fitness(userInput);    // A function(object) that assigns an evaluation score, given a hypothesis.

                int Fitness_threshold = fit.maxFit();   // A threshold specifying the termination criterion.
                //int p = 100; // The number of hypotheses to be included in the population.
                //double r = 0.25;    // The fraction of the population to be replaced by Crossover at each step.
                //double m = 0.10;    // The mutation rate.

                Console.Write("\nEnter integer p, the initial number of hypotheses (e.g. 100): ");
                string pIn = Console.ReadLine();
                int p = int.Parse(pIn);

                Console.Write("\nEnter double r, fraction to be replaced by crossover (e.g. 0.25): ");
                string rIn = Console.ReadLine();
                double r = double.Parse(rIn);

                Console.Write("\nEnter double m, the mutation rate (e.g. 0.10): ");
                string mIn = Console.ReadLine();
                double m = double.Parse(mIn);

                GA ga = new GA(fit, Fitness_threshold, p, r, m);
                ga.init_P();
                ga.evaluate();
                ga.runGA();

                Console.Write("\nAgain? 'y' or 'n': ");
                choice = Console.ReadLine();
            } while (choice != "n");

            //int p = 160;
            //while (p > 50)
            //{
            //    Fitness fit = new Fitness("test");    // A function(object) that assigns an evaluation score, given a hypothesis.

            //    int Fitness_threshold = fit.maxFit();   // A threshold specifying the termination criterion.
            //    //int p = 100; // The number of hypotheses to be included in the population.
            //    double r = 0.25;    // The fraction of the population to be replaced by Crossover at each step.
            //    double m = 0.10;    // The mutation rate.

            //    GA ga = new GA(fit, Fitness_threshold, p, r, m);
            //    ga.init_P();
            //    ga.evaluate();
            //    ga.runGA();

            //    p -= 10;
            //}

            //Console.ReadKey();
        }
    }
}
