﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace GeneticAlgorithms
{
    public class Fitness
    {
        private string goal;

        public Fitness(string str)
        {
            this.goal = str;
        }

        public int fit_score(string hyp)
        {
            int fit = 0;
            int i = 0;
            foreach (char c in hyp)
            {
                if (c == goal[i])
                    fit++;
                i++;
            }
            return fit;
        }

        public string goalCheck()
        {
            return goal;
        }

        public int maxFit()
        {
            return goal.Length;
        }
    }
}
